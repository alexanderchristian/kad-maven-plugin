package de.root1.maven.plugin.kad;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

/**
 * Says "Hi" to the user.
 *
 */
@Mojo(name = "deploylocal", defaultPhase = LifecyclePhase.DEPLOY)
@Execute(goal = "assemble")
public class KadDeployLocalMojo extends AbstractMojo {

    @Parameter(property = "kad.deployfolder")
    private File deployfolder;

    @Parameter(defaultValue = "${project.build.directory}", required = true)
    private File targetFolder;

    /**
     * Practical reference to the Maven project
     */
    @Parameter(defaultValue = "${project}", readonly = true)
    private MavenProject project;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        if (deployfolder != null && deployfolder.exists()) {

            File[] plugins = targetFolder.listFiles(new FileFilter() {
                @Override
                public boolean accept(File f) {
                    return f.getName().endsWith("-kadplugin.jar");
                }
            });

            for (File plugin : plugins) {
                try {
                    File target = new File(deployfolder, plugin.getName());
                    getLog().info("deploying " + plugin.getAbsolutePath() + " to target plugin folder " + target.getAbsolutePath());
                    //Files.copy(plugin.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    copyFileUsingStream(plugin, target);
                } catch (IOException ex) {
                    Logger.getLogger(KadDeployLocalMojo.class.getName()).log(Level.SEVERE, "Error copying plugin", ex);
                }
            }
        } else {
            getLog().error("target plugin folder " + deployfolder + " does not exist. Skipping.");
        }

    }

    private static void copyFileUsingStream(File source, File dest) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            is.close();
            os.close();
        }
    }

}
