/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.root1.maven.plugin.kad;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.Stack;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.zip.ZipException;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.descriptor.PluginDescriptor;
import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

/**
 *
 * @author achristian
  */
@Mojo(name = "assemble", requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME, defaultPhase = LifecyclePhase.PACKAGE)
@Execute(phase = LifecyclePhase.PACKAGE)
public class KadAssembleMojo extends AbstractMojo {

    private final int BUFFER = 2048;

    @Parameter(property = "kad.pluginproperties", defaultValue = "plugin.properties")
    private String pluginproperties;
    
    @Parameter(defaultValue = "${project.build.directory}", required = true)
    private String targetFolder;

    @Parameter(defaultValue = "${localRepository}", readonly = true, required = true)
    private ArtifactRepository local;

    @Parameter(defaultValue = "${project.compileClasspathElements}", required = true, readonly = true)
    private List<String> compileClasspath;

    @Parameter(defaultValue = "${project}", readonly = true)
    private MavenProject project;

    @Parameter(defaultValue = "${plugin}", readonly = true)
    private PluginDescriptor pluginDescriptor;

    /**
     * Generates a URL[] with the project class path (can be used by a
     * URLClassLoader)
     *
     * @return the array of classpath URL's
     * @throws MojoExecutionException
     */
    private URL[] generateClassPathUrls() throws MojoExecutionException {
        List<URL> urls = new ArrayList<>();
        URL url;
        try {
            for (Object element : compileClasspath) {
                String path = (String) element;
                if (path.endsWith(".jar")) {
                    url = new URL("jar:" + new File(path).toURI().toString() + "!/");
                } else {
                    url = new File(path).toURI().toURL();
                }
                urls.add(url);
            }
        } catch (MalformedURLException e) {
            throw new MojoExecutionException("Could not set up classpath", e);
        }
        return urls.toArray(new URL[urls.size()]);
    }
    
    // https://community.oracle.com/blogs/malenkov/2008/07/25/how-load-classes-jar-or-zip
    public boolean createJarArchive(File srcFolder, Set<Artifact> dependencies, File outputfile) throws MojoExecutionException {

        try {
            
            getLog().info("outfile: "+outputfile.getAbsolutePath());
                            
            BufferedInputStream bis = null;

            FileOutputStream dest = new FileOutputStream(outputfile);

            Properties pluginProps = new Properties();
            pluginProps.load(new FileReader(pluginproperties));
            
            Manifest manifest = new Manifest();
            Attributes attr = manifest.getMainAttributes();
            attr.put(Attributes.Name.MANIFEST_VERSION, "1.0");
            attr.putValue("Kad-Plugin-Author", pluginProps.getProperty("kad.plugin.author"));
            attr.putValue("Kad-Plugin-Id", pluginProps.getProperty("kad.plugin.id"));
            attr.putValue("Kad-Plugin-Version", pluginProps.getProperty("kad.plugin.version"));
            
            Iterator<Object> keySetIter = attr.keySet().iterator();
            while (keySetIter.hasNext()) {
                Attributes.Name key = (Attributes.Name) keySetIter.next();
                getLog().info("Manifest: "+key+"="+attr.getValue(key));
            }
            
            JarOutputStream out = new JarOutputStream(new BufferedOutputStream(dest), manifest);
            byte data[] = new byte[BUFFER];

            Stack<File> stack = new Stack();
            for (File f : srcFolder.listFiles()) {
                stack.push(f);
            }

            // find matching class for interface de.root1.spf.PluginInterface
            List<String> pluginServices = new ArrayList<>();
            URLClassLoader classLoader = new URLClassLoader(generateClassPathUrls());
            String serviceClassName = "de.root1.spf.PluginInterface";
            Class<?> serviceClass = null;
            try {
                serviceClass = classLoader.loadClass(serviceClassName);
            } catch (ClassNotFoundException ex) {
                throw new MojoExecutionException("Could not load class: " + serviceClassName, ex);
            }

            while (!stack.isEmpty()) {
                File f = stack.pop();
                String entryName = f.getAbsolutePath().substring(srcFolder.getAbsolutePath().length()).substring(1);
                if (f.isDirectory()) {
                    // FOLDER
                    entryName+="/";
                    JarEntry entry = new JarEntry(entryName);
                    out.putNextEntry(entry);
                    getLog().info("Adding folder entry: ["+entryName+"]");
                    getLog().info("Diving into subfolder: " + f.getAbsolutePath());
                    for (File subfile : f.listFiles()) {
                        stack.push(subfile);
                    }
                    
                } else {
                    // FILE
                    bis = new BufferedInputStream(new FileInputStream(f), BUFFER);
                    getLog().info("Adding to archive: " + f.getAbsolutePath() + " -> [" + entryName+"]");
                    JarEntry entry = new JarEntry(entryName);
                    out.putNextEntry(entry);
                    int count;
                    while ((count = bis.read(data, 0, BUFFER)) != -1) {
                        out.write(data, 0, count);
                        out.flush();
                    }
                    bis.close();

                    // check for service file
                    try {

                        if (entryName.endsWith(".class")) {
                            entryName = entryName.replace(File.separator, ".");
                            entryName = entryName.replaceAll(".class", "");// remove .class extension

                            getLog().info("checking class: " + entryName);

                            Class<?> cls = classLoader.loadClass(entryName);
                            int mods = cls.getModifiers();
                            if (!cls.isAnonymousClass() && !cls.isInterface()
                                    && !cls.isEnum() && !Modifier.isAbstract(mods)
                                    && Modifier.isPublic(mods)) {
                                if (!serviceClass.equals(cls) && serviceClass.isAssignableFrom(cls)) {
                                    pluginServices.add(entryName);
                                    getLog().warn("Adding service class: " + entryName);

                                }
                            }
                        }
                    } catch (ClassNotFoundException ex) {
                        getLog().warn(ex);
                    }
                    // end check
                }
            }

            // add service
            JarEntry serviceEntry = new JarEntry("META-INF/services/de.root1.spf.PluginInterface");
            out.putNextEntry(serviceEntry);
            for (String pluginService : pluginServices) {
                out.write((pluginService + "\n").getBytes());
            }
            out.flush();

            // add dependencies
            for (Artifact unresolvedArtifact : dependencies) {

                if (unresolvedArtifact.getScope().equals("compile")) {
                    // Find the artifact in the local repository.
                    Artifact art = local.find(unresolvedArtifact);

                    File dependencyJar = art.getFile();
                    getLog().info("dependencyJar=" + dependencyJar);
                    getLog().info("Add dependency; jar=" + dependencyJar.getName());

                    JarFile jar = new JarFile(dependencyJar);
                    Enumeration<? extends JarEntry> jarEntries = jar.entries();
                    while (jarEntries.hasMoreElements()) {
                        JarEntry entry = new JarEntry(jarEntries.nextElement());

                        try {
                            out.putNextEntry(entry);
                            BufferedInputStream entryBis = new BufferedInputStream(jar.getInputStream(entry));
                            getLog().info("Add dependency; file=" + entry);

                            int bytesRead = 0;
                            while ((bytesRead = entryBis.read(data)) != -1) {
                                out.write(data, 0, bytesRead);
                                out.flush();
                            }
                            entryBis.close();
                        } catch (ZipException e) {
                            getLog().warn("skipping due to: " + e.getMessage());
                        }

                    }
                    // ...
                }
            }

            out.flush();
            out.close();
        } catch (IOException e) {
            getLog().error("createJarArchive threw exception: " + e.getMessage());
            e.printStackTrace();
            return false;

        }

        return true;
    }

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {

        File classes = new File(targetFolder, "classes");

        String name = project.getArtifactId() + "-" + project.getVersion() + "-kadplugin.jar";
        createJarArchive(classes, project.getDependencyArtifacts(), new File(targetFolder, name));

    }

}
